import React, { Component } from 'react';
import './styles/App.css';
import {stocks} from './data/StockSamples.js';
import {Stocks} from './components/Stocks.js';
import {TradeHistory} from './components/TradeHistory.js';

class App extends Component {
    constructor() {
        super();
        this.state = {
            stocksData:[],
            stocksHistory: [],
            errorMessage: "",
            AllShareIndex: 0
        };
    }

    render() {
        return (
            <div>
                <AppHeader text="Global Beverage Corporation"/>
                <Stocks
                    stocksData={this.state.stocksData}
                    transactionFunction={this.handleTransaction.bind(this)}
                    errorHandlingFunction={this.errorHandling.bind(this)}
                />
                <TradeHistory ref="history" tradeHistory={this.state.stocksHistory}/>
                <div className="Errors">{this.state.errorMessage}</div>
                <div className="Button Calculate" onClick={this.calculateAllShareIndex.bind(this)}>Update All Shares Index</div>
                <div className="AllShareIndex">{this.state.AllShareIndex}</div>
            </div>
        );
    }

    componentDidMount() {
        /* Currently only "fetches" data, put in place in case a future app needs more stuff on mountpoint */
        this.getStocksData();
    }

    getStocksData() {
        /* This function mocks an API call to get actual database information */
        this.setState({
            stocksData: stocks,
        });
    }

    handleTransaction(type) {
        let newStockHistory = this.state.stocksHistory.concat();
        newStockHistory.push(type);
        this.setState({
            stocksHistory: newStockHistory
        });
    }

    calculateAllShareIndex(symbol, value) {
        let values = [].slice.call(document.querySelectorAll(".isSet.PlaceholderField.VWSP"));
        let PartialAllShareIndex = 1;
        values.map((key) => {
            PartialAllShareIndex *= parseInt(key.innerHTML)
        });
        let AllShareIndex = Math.pow(PartialAllShareIndex, 1/values.length);
        this.setState({
            AllShareIndex: AllShareIndex
        })
    }

    errorHandling(error) {
        this.setState({
            errorMessage: error
        });
    }
}

class AppHeader extends Component {
    render() {
        return (
            <div className="TitleContainer">
                <h1 className="AppTitle">{this.props.text}</h1>
                <h3 className="Heading">Super Simple Stocks Manager</h3>
            </div>
        )
    }
}

export default App;
