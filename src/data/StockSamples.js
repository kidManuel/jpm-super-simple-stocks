export const stocks = [
    {
        "symbol": "TEA",
        "preferred": false,
        "lastDividend": 0,
        "fixedDividend": null,
        "parValue": 100
    },{
        "symbol": "POP",
        "preferred": false,
        "lastDividend": 8,
        "fixedDividend": null,
        "parValue": 100
    },{
        "symbol": "ALE",
        "preferred": false,
        "lastDividend": 23,
        "fixedDividend": null,
        "parValue": 60
    },{
        "symbol": "GIN",
        "preferred": true,
        "lastDividend": 8,
        "fixedDividend": 2,
        "parValue": 100
    },{
        "symbol": "JOE",
        "preferred": false,
        "lastDividend": 13,
        "fixedDividend": null,
        "parValue": 250
    }
]
