import React, { Component } from 'react';
import {StockRow} from './StockRow.js';
import './Stocks.css';

export class Stocks extends Component {
    render() {
        return (
            <div className="Stocks">
                <div className="StocksHeader">Current Stocks</div>
                <div className="StocksContainer">
                    {
                        /* Add a stock element based on fetched data */
                        this.props.stocksData.map(
                            (stock, index) => <StockRow
                                text={stock.symbol}
                                preferred={stock.preferred}
                                lastDividend={stock.lastDividend}
                                fixedDividend={stock.fixedDividend}
                                parValue={stock.parValue}
                                transactionFunction={this.props.transactionFunction}
                                errorHandlingFunction={this.props.errorHandlingFunction}
                                key={index}
                            />
                        )
                    }
                </div>
            </div>
        );
    }
}
