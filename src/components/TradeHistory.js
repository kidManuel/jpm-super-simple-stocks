import React, { Component } from 'react';
import './TradeHistory.css';

export class TradeHistory extends Component {
    render() {
        return (
            <div className="RecordHistory">
                <div className="HistoryHeader">History</div>
                <div className="TradeHistoryTitle"></div>
                {
                    this.props.tradeHistory.map((key, index) => {
                        return <SingleTrade tradeInfo={key} key={index}/>
                    })
                }
            </div>
        )
    }
}

class SingleTrade extends Component {
    constructor(props) {
        super(props);
        this.state = {TradeType: ""}
    }

    componentDidMount() {
        this.setState({
            TradeType: this.props.tradeInfo.Buying ? "Buying" : "Selling"
        })
    }

    render() {
        return (
            <div className="TradeHistoryContainer">
                <div className={this.state.TradeType + " " + "TradeRow"}>
                    <div className="TradeTypeIndicator"></div>
                    <TradeField label="Symbol" value={this.props.tradeInfo.Symbol}/>
                    <TradeField label="Type" value={this.state.TradeType}/>
                    <TradeField label="Time" value={this.props.tradeInfo.Timestamp.getHours() + ':' + this.props.tradeInfo.Timestamp.getMinutes()}/>
                    <TradeField label="Quantity" value={this.props.tradeInfo.Ammount}/>
                    <TradeField label="At price" value={this.props.tradeInfo.Price}/>
                </div>
            </div>
        )
    }
}

class TradeField extends Component {
    render() {
        return (
            <div className="TradeField">
                <span className="TradeFieldLabel">{this.props.label + ':'}</span>
                <span className="TradeFieldValue">{this.props.value}</span>
            </div>
        )
    }
}
