import React, { Component } from 'react';
import './StockRow.css';
import {TimeHelper} from './TimeHelper.js'
import {PlaceholderField} from './PlaceholderField.js'

export class StockRow extends Component {
    constructor(props) {
        super(props);
        this.onPriceChange = this.onPriceChange.bind(this);
        this.state = {
            DividendYield: '',
            PriceSet: false,
            PERatio: '',
            Ammount: 0,
            Price: 0,
            TradesOfTypeHistory: [],
            VolumeWeightedStockPrice: '',
            CONFIG_DECIMAL_POINTS: 3
        }
    }

    render() {
        return (
            <div className="StockRow">
                <div className="StockName">{this.props.text}</div>
                <input
                    type="number"
                    placeholder="Trading Price"
                    onChange={this.onPriceChange.bind(this)}
                    className="PriceInput"
                />
                <PlaceholderField className="DividendYield" Text={this.state.DividendYield} PlaceholderText="DividendYield" />
                <PlaceholderField className="PERatio" Text={this.state.PERatio} PlaceholderText="P/E Ratio" />
                <input
                    type="number"
                    placeholder="Ammount"
                    onChange={this.onAmmountChange.bind(this)}
                    className="AmmountInput"
                />

                <div className="TransactionButtons">
                    <div className="Button Buy" onClick={() => this.reportTransaction(true)}>Buy!</div>
                    <div className="Button Sell" onClick={() => this.reportTransaction(false)}>Sell!</div>
                </div>
                <PlaceholderField extraCssClass="VWSP" Text={this.state.VolumeWeightedStockPrice} PlaceholderText="VWSP" />
            </div>
        )
    }

    onPriceChange(e){
        var currentPrice = e.target.value;
        if (currentPrice === '') {
            this.setState({
                PriceSet: false,
                DividendYield: '',
                PERatio: ''
            })
        } else {
            var newDividendYield = this.calculateDividendYield(currentPrice);
            var newPERatio = this.calculatePriceEarningsRatios(currentPrice);
            this.setState({
                Price: currentPrice,
                PriceSet: true,
                DividendYield: newDividendYield,
                PERatio: newPERatio
            });
        }
    }

    onAmmountChange(e){
        var newAmmount = e.target.value;
        this.setState({
            Ammount: newAmmount
        });
    }

    calculateDividendYield(value) {
        var partialValue = 0;
        if (value){
            if (this.props.preferred) {
                partialValue = (((this.props.fixedDividend / 100) * this.props.parValue) / value)
            } else {
                partialValue = (this.props.lastDividend / value)
            }
        }
        return partialValue.toFixed(this.state.CONFIG_DECIMAL_POINTS);
    }

    calculatePriceEarningsRatios(value) {
        if (value && this.props.lastDividend) {
            return (value / this.props.lastDividend).toFixed(this.state.CONFIG_DECIMAL_POINTS);
        } else {
            return "-";
        }
    }

    reportTransaction(Buying) {
        if (this.state.Ammount && this.state.Price) {
            var transactionData = {
                    Symbol: this.props.text,
                    Timestamp: new Date(),
                    Buying: Buying,
                    Ammount: this.state.Ammount,
                    Price: this.state.Price
            };
            this.handleTransactionOfType(transactionData);
            this.props.transactionFunction(transactionData);
            this.setError('');
        } else {
            this.setError('Please fill all inputs!')
        }
    }

    handleTransactionOfType(singleTransaction) {
        let newStockHistory = this.state.TradesOfTypeHistory.concat();
        newStockHistory.push(singleTransaction);
        this.setState({
            TradesOfTypeHistory: newStockHistory,
        }, () => {
            this.calculateVolumeWeightedStockPrice()
        });
    }

    calculateVolumeWeightedStockPrice() {
        let now = new Date();
        let dividend = 0;
        let divisor = 0;
        this.state.TradesOfTypeHistory.map((Transaction) => {
            if(TimeHelper.isDateInRange(Transaction.Timestamp, now)) {
                dividend += (Transaction.Price * Transaction.Ammount);
                divisor += Transaction.Ammount;
            }
        })
        var newVolumeWeightedStockPrice = (parseInt(dividend) / parseInt(divisor)).toFixed(this.state.CONFIG_DECIMAL_POINTS);
        this.setState({
            VolumeWeightedStockPrice: newVolumeWeightedStockPrice
        });
    }

    setError(errorMessage) {
        this.props.errorHandlingFunction(errorMessage)
    }
}
