import React, { Component } from 'react';
import './PlaceholderField.css';

export class PlaceholderField extends Component {
    render() {
        return (
            <div className={(this.props.Text === '' ? "notSet" : "isSet") + " " + "PlaceholderField " + this.props.extraCssClass}>
                {this.props.Text === '' ? this.props.PlaceholderText : this.props.Text}
            </div>
        )
    }
}
