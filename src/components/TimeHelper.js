export class TimeHelper {
    static isDateInRange(date, now) {
        return (now.getTime() - date.getTime()) < (TimeHelper.CONFIG_TIME_IN_MINUTES * 60000) 
    }
}

TimeHelper.CONFIG_TIME_IN_MINUTES = 5;
